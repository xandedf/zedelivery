<?php
/**
 * Phalcon Software LTDA
 *
 * @copyright     Copyright (c) 2019 Phalcon Software LTDA.
 * @category      Phalcon Software
 *
 * @author        alexandregomes <xandergomes@gmail.com>
 */

/**
 * API Routes
 */
$app->get('/', function () use ($app) {
    echo $app['view']->render('index');
});

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->render('404');
});

/**
 * Route test from application
 */
$app->get(
    "/ping/{content}",
    function ($content) {
        echo "<h1>Pong => $content</h1>";
    }
);

/**
 * Include PDVS Routes
 */
include APP_PATH . '/includes/partner.php';

/**
 * Include User
 */
//include APP_PATH . '/includes/user.php';

/**
 * Include Transactions
 */
//include APP_PATH . '/includes/transactions.php';

/**
 * Include Transactions
 */
//include APP_PATH . '/includes/notifications.php';

