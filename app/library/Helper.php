<?php
/**
 * Phalcon Software LTDA
 *
 * @copyright     Copyright (c) 2019 Phalcon Software LTDA.
 * @category      Phalcon Software
 *
 * @author        alexandregomes <xandergomes@gmail.com>
 */

class Helper {

    /**
     * @param $cnpj
     * @return bool
     */
    static function validaCnpj($cnpj)
    {
        $cnpj = preg_replace('/[^0-9]/', '', $cnpj);
        $cnpj = (string)$cnpj;
        $cnpjOriginal = $cnpj;

        $primeirosNumerosCnpj = substr($cnpj, 0, 12);

        if (!function_exists('multiplica_cnpj')) {
            function multiplica_cnpj($cnpj, $posicao = 5)
            {
                $calculo = 0;

                for ($i = 0; $i < strlen($cnpj); $i++) {

                    $calculo = $calculo + ($cnpj[$i] * $posicao);
                    $posicao--;

                    if ($posicao < 2) {
                        $posicao = 9;
                    }
                }
                return $calculo;
            }
        }

        $primeiroCalculo = multiplica_cnpj($primeirosNumerosCnpj);

        $primeiroDigito = ($primeiroCalculo % 11) < 2 ? 0 : 11 - ($primeiroCalculo % 11);

        $primeirosNumerosCnpj .= $primeiroDigito;

        $segundoCalculo = multiplica_cnpj($primeirosNumerosCnpj, 6);
        $segundoDigito = ($segundoCalculo % 11) < 2 ? 0 : 11 - ($segundoCalculo % 11);

        $cnpj = $primeirosNumerosCnpj . $segundoDigito;

        if ($cnpj === $cnpjOriginal) {
            return true;
        }

        return false;
    }
}