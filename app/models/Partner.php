<?php
/**
 * Phalcon Software LTDA
 *
 * @copyright     Copyright (c) 2019 Phalcon Software LTDA.
 * @category      Phalcon Software
 *
 * @author        alexandregomes <xandergomes@gmail.com>
 */

use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Manager;
use Phalcon\Http\Response;

class Partner extends ModelBase
{

    protected $_source = "partner";

    const PARTNER_NOT_FOUND_ERROR = "Partner not found.";
    const INVALID_DATA = "The data has invalid, all fields are mandatory.";
    const INVALID_DOCUMENT = "The document has invalid";
    const INVALID_EXIST_DOCUMENT = "Document is already registered";
    const CODE_ERROR_PARTNER_SETUP = 3;
    const SETUP_SUCCESS = "Setup Success";
    const PARTNER_SETUP_ERROR = "Error while trying to setup";

    protected $_fields = ["id", "tradingName", "ownerName", "document", "coverageArea", "address"];
    protected $_fieldsGeoJson = ['type','coordinates'];

    public function getSource() {
        return $this->_source;
    }

    /**
     * @param $data
     * @throws \MongoDB\Driver\Exception\Exception
     */
    private function _checkData($data) {

        try {

            foreach ($this->_fields as $field) {

                if (!isset($data->$field) || $data->$field === "") {
                    throw new Exception(self::INVALID_DATA);
                }

                if ($field === 'document' && !Helper::validaCnpj($data->$field)) {
                    throw new Exception(self::INVALID_DOCUMENT);
                }

                if ($field === 'document') {
                    $document = $this->getPartnerByField('document', $data->$field, true, true);
                    if (!isset($document->code)) {
                        throw new Exception(self::INVALID_EXIST_DOCUMENT);
                    }
                }

                if (($field === 'coverageArea' || $field === 'address') && $data->$field !== "") {
                    foreach ($this->_fieldsGeoJson as $fieldGeoJason) {
                        if (!isset($data->$field->$fieldGeoJason) || $data->$field->$fieldGeoJason === "") {
                            throw new Exception(self::INVALID_DATA);
                        }
                    }
                }

            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param $field
     * @param $value
     * @param bool $onlyFirst
     * @param bool $returnObject
     * @return array|bool|mixed|Response
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function getPartnerByField($field, $value, $onlyFirst = false, $returnObject = false) {

        try {

            $partnerFound = $onlyFirst ? $this->returnFirst([$field => $value]) : $this->returnAll([$field => $value]);

            if ($partnerFound == false)  {
                throw new Exception(self::PARTNER_NOT_FOUND_ERROR, self::CODE_ERROR_SEARCH);
            } else {
                $data = array(
                    'status' => self::FOUND_SUCCESS,
                    'data' => $partnerFound
                );
            }

            if ($returnObject == false) {
                $response = new Response();
                $response->setJsonContent($data);
                return $response;
            }

            return $partnerFound;

        } catch(Exception $e) {
            $data = array(
                'code'  => self::CODE_ERROR_SEARCH,
                'status' => self::ERROR,
                'message' => $e->getMessage()
            );

            if ($returnObject == false) {
                $response = new Response();
                $response->setStatusCode(503, self::ERROR);
                $response->setJsonContent($data);
                return $response;
            }
            return json_decode(json_encode($data));
        }

    }

    /**
     * @param array|null $coordinates
     * @param bool $returnObject
     * @return array|bool|mixed|Response
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function getCoveragePartners(array $coordinates = null, $returnObject = false) {

        try {

            $filter = ['coverageArea' => ['$geoIntersects' => ['$geometry' => ['type' => 'Point', 'coordinates' => $coordinates]]]];
            $partnersFound = $this->returnAll($filter);

            if ($partnersFound == false)  {
                throw new Exception(self::PARTNER_NOT_FOUND_ERROR, self::CODE_ERROR_SEARCH);
            } else {
                $data = array(
                    'status' => self::FOUND_SUCCESS,
                    'data' => $partnersFound
                );
            }

            if ($returnObject == false) {
                $response = new Response();
                $response->setJsonContent($data);
                return $response;
            }

            return $partnersFound;

        } catch(Exception $e) {
            $data = array(
                'code'  => self::CODE_ERROR_SEARCH,
                'status' => self::ERROR,
                'message' => $e->getMessage()
            );

            if ($returnObject == false) {
                $response = new Response();
                $response->setStatusCode(503, self::ERROR);
                $response->setJsonContent($data);
                return $response;
            }
            return json_decode(json_encode($data));
        }
        
    }

    /**
     * @param $data
     * @param bool $returnObject
     * @return mixed|Response
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function insertNewPartner($data, $returnObject = false) {

        try {

            $this->_checkData($data);

            $return = $this->insertOne($data);

            if ($return->getInsertedCount() == 0) {
                foreach ($this->getMessages() as $message) {
                    throw new Exception($message);
                }
            } else {
                $data = array(
                    'status' => 'OK',
                    'data'   => $data
                );
            }

            if ($returnObject == false) {
                $response = new Response();
                $response->setStatusCode(201, "Created");
                $response->setJsonContent($data);
                return $response;
            }

            return json_decode(json_encode($data));

        } catch(Exception $e) {
            $data = array(
                'code'  => self::CODE_ERROR_ADD,
                'status' => self::ERROR,
                'message' => $e->getMessage()
            );

            if ($returnObject == false) {
                $response = new Response();
                $response->setStatusCode(503, self::ERROR);
                $response->setJsonContent($data);
                return $response;
            }
            return json_decode(json_encode($data));
        }

    }

    /**
     * @param bool $returnObject
     * @return mixed|Response
     */
    public function setup($returnObject = false) {

        try {

            $file = file_get_contents('../config/partners.json');
            $file = json_decode($file);
            $returnInsert = $this->insertMany('document', $file);

            if ($returnInsert == false)  {
                throw new Exception(self::PARTNER_SETUP_ERROR, self::CODE_ERROR_PARTNER_SETUP);
            } else {
                $data = array(
                    'status' => self::SETUP_SUCCESS,
                    'data' => "{$returnInsert->getUpsertedCount()} documents inserted at database {$this->_getSourceName(true)}"
                );
            }

            if ($returnObject == false) {
                $response = new Response();
                $response->setJsonContent($data);
                return $response;
            }

        } catch(Exception $e) {
            $data = array(
                'code'  => self::CODE_ERROR_PARTNER_SETUP,
                'status' => self::ERROR,
                'message' => $e->getMessage()
            );
            return json_decode(json_encode($data));
        }
    }
}