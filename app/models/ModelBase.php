<?php
/**
 * Phalcon Software LTDA
 *
 * @copyright     Copyright (c) 2019 Phalcon Software LTDA.
 * @category      Phalcon Software
 *
 * @author        alexandregomes <xandergomes@gmail.com>
 */


use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Query;
use Phalcon\Mvc\Collection;

class ModelBase extends Collection
{

    /**
     * ERROR Codes
     */
    const CODE_ERROR_SEARCH = 1;
    const CODE_ERROR_ADD    = 2;

    /**
     * MSG Errors
     */
    const ERROR = 'Error';

    /**
     * MSG Success
     */
    const FOUND_SUCCESS = 'Found';

    protected $_config;
    protected $_source;
    protected $_mongo;
    protected $_environment;

    public function initialize() {
        $this->_mongo = self::getDI()->getMongo();
        $this->_config = self::getDI()->getConfig();
        $this->_source = self::getSource();
        $this->_environment = $this->_config->application->environment;
    }

    /**
     * @param bool $fullPath
     * @return string
     */
    protected function _getSourceName($fullPath = false) {
        $environment = $this->_environment;
        return $fullPath ? "{$this->_config->mongo->$environment->dbname}.{$this->_source}" : $this->_source;
    }

    /**
     * @param array|null $parameters
     * @return \MongoDB\Driver\Cursor
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function execute(array $parameters = null) {

        $query = new Query($parameters);
        return $this->_mongo->executeQuery($this->_getSourceName(true), $query);
    }

    /**
     * @param null $data
     * @return \MongoDB\Driver\WriteResult
     */
    public function insertOne($data = null) {

        $mongoInsert = new BulkWrite();
        $mongoInsert->insert($data);
        return $this->_mongo->executeBulkWrite($this->_getSourceName(true), $mongoInsert);
    }

    /**
     * @param string $idField
     * @param null $data
     * @return mixed
     */
    public function insertMany($idField = '', $data = null) {

        $mongoInsert = new BulkWrite();
        foreach ($data as $document) {
            $mongoInsert->update(
                [$idField => $document->$idField],
                $document,
                ['multi' => false, 'upsert' => true]
            );
        }
        return $this->_mongo->executeBulkWrite($this->_getSourceName(true), $mongoInsert);

    }

    /**
     * @param array|null $parameters
     * @return bool
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function returnFirst(array $parameters = null) {

        $document = false;
        $rows = $this->execute($parameters);
        foreach ($rows as $document) {
            break;
        }
        return $document;
    }

    /**
     * @param array|null $parameters
     * @return array|bool
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function returnAll(array $parameters = null) {

        $documents = [];
        $rows = $this->execute($parameters);
        foreach ($rows as $document) {
            $documents[] = $document;
        }
        return count($documents) > 0 ? $documents : false;
    }

}