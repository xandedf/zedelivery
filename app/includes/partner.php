<?php
/**
 * Phalcon Software LTDA
 *
 * @copyright     Copyright (c) 2019 Phalcon Software LTDA.
 * @category      Phalcon Software
 *
 * @author        alexandregomes <xandergomes@gmail.com>
 */


/**
 * Search for partner by id (GET)
 * /partner/{id}
 */
$app->get('/partner/{id}', function ($id) use ($app) {
    $partner = new Partner();
    return $partner->getPartnerByField('id', (string)$id, true);
});

/**
 * Return partners near from coordinates (GET)
 * /partner/coverage/{coordinates}
 */
$app->get('/partner/coverage/{coordinates}', function ($coordinates) use ($app) {
    $coordinates = array_map('floatval', explode(',',$coordinates));
    $partner = new Partner();
    return $partner->getCoveragePartners($coordinates);
});

/**
 * Create a new partner (POST)
 * '/partner'
 */
$app->post('/partner', function () use ($app) {
    $data = $app->request->getJsonRawBody();
    $partner = new Partner();
    return $partner->insertNewPartner($data);
});

/**
 * Create a sample data for partners (GET)
 * '/partner/setup'
 */
$app->get('/partner/setup', function () use ($app) {
    $partner = new Partner();
    return $partner->setup();
});

