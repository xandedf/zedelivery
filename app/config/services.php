<?php
/**
 * Phalcon Software LTDA
 *
 * @copyright     Copyright (c) 2019 Phalcon Software LTDA.
 * @category      Phalcon Software
 *
 * @author        alexandregomes <xandergomes@gmail.com>
 */

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\View\Simple as View;
use Phalcon\Mvc\Url as UrlResolver;
use MongoDB\Driver\Manager;

$di = new FactoryDefault();

$di->set('config', function() use ($config) {
    return $config;
}, true);

/**
 * Sets the view component
 */
$di['view'] = function () use ($config) {
    $view = new View();
    $view->setViewsDir($config->application->viewsDir);
    return $view;
};

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di['url'] = function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
};

/**
 * Set the collectionManager
 */
$di->set('collectionManager', function(){
    return new Phalcon\Mvc\Collection\Manager();
}, true);

/**
 * Mongo DB
 */
$di->set('mongo', function () use ($config) {
    $environment = $config->application->environment;
    $strConnect = (!$config->mongo->$environment->username) ? "mongodb://{$config->mongo->$environment->host}" : "mongodb://{$config->mongo->$environment->username}:{$config->mongo->$environment->password}@{$config->mongo->$environment->host}";
    return new Manager($strConnect);
}, true);
