<?php
/**
 * Phalcon Software LTDA
 *
 * @copyright     Copyright (c) 2019 Phalcon Software LTDA.
 * @category      Phalcon Software
 *
 * @author        alexandregomes <xandergomes@gmail.com>
 */

/**
 * Registering an autoloader
 */

use Phalcon\Loader;

$loader = new Loader();

$loader->registerDirs(
    array(
        $config->application->modelsDir,
        $config->application->libraryDir
    )
)->register();
