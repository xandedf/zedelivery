<?php
/**
 * Phalcon Software LTDA
 *
 * @copyright     Copyright (c) 2019 Phalcon Software LTDA.
 * @category      Phalcon Software
 *
 * @author        alexandregomes <xandergomes@gmail.com>
 */

use Phalcon\Config;

return new Config(array(
    'mongo'  => array(
        'sandbox' => array(
            'host'     => 'mongo-db:27017',
            'username' => 'root',
            'password' => 'mongo1234',
            'dbname'   => 'zedelivery'
        ),
        'production' => array(
            'host' => 'ds121406.mlab.com:21406/zedelivery',
            'username' => 'zedelivery',
            'password' => 'mongo1234',
            'dbname'   => 'zedelivery'
        )
    ),
    'application' => array(
        'version'        => '1.0.0',
        'modelsDir'      => APP_PATH . '/models/',
        'viewsDir'       => APP_PATH . '/views/',
        'libraryDir'     => APP_PATH . '/library/',
        'baseUri'        => '/app/',
        'environment'    => 'production' //sandbox ou production
    )
));
