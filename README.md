# Zé Delivery API

API developed for ZX Ventures Backend Challenge, based on the documentation and criteria provided.

With this API we are able to register sales points, return a point of sale and search for nearby points based on a geographical location (longitute and latitude)

API developed with Phalcon PHP and MongoDB technology

## Installation
For easier installation, I created a Docker container with all the necessary environment to run the application.
Dependencies:
  * Docker engine v1.13 or higher. Your OS provided package might be a little old, if you encounter problems, do upgrade. See [https://docs.docker.com/engine/installation](https://docs.docker.com/engine/installation)
  * Docker compose v1.12 or higher. See [docs.docker.com/compose/install](https://docs.docker.com/compose/install/)

After installing the dependencies and unzipping the application .zip file to your working directory, open the terminal and follow the steps below

Starting the Docker containers (the first time will take a while to load)
```sh
$ cd <folder>
$ ./start
```

## Prepare database
To include sample data in the database, simply access the URL below in the browser
```sh
http://localhost:8080/partner/setup
```

## Application configuration
If you need to change the settings of the application as environment or connections to the MongoDB, this can be done through config.php
```sh
$ /app/config/config.php
```

## API Usage

```sh
(GET)http://localhost:8080/partner/{id} //Get partner by id
```
```sh
(GET)http://localhost:8080/partner/coverage/{coordinates} //Get partners from coordinates
{coordinates} = -46.684109,-23.617893 (longitude,latitude)
```
```sh
(POST)http://localhost:8080/partner //Insert new partner
```
### Payload
This the payload to new partner (every fields are mandatory)
```sh
    {
        "id": 1, 
        "tradingName": "Adega da Cerveja - Pinheiros",
        "ownerName": "Zé da Silva",
        "document": "1432132123891/0001", //CNPJ
        "coverageArea": { 
          "type": "MultiPolygon", 
          "coordinates": [
            [[[30, 20], [45, 40], [10, 40], [30, 20]]], 
            [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]]
          ]
        }, //Área de Cobertura
        "address": { 
          "type": "Point",
          "coordinates": [-46.57421, -21.785741]
        }, // Localização do PDV
    }
```

## Services exposed outside your environment ##

You can access application via **`localhost`**, if you're running the containers directly, or through **``** when run on a vm. nginx and mailhog both respond to any hostname, in case you want to add your own hostname on your `/etc/hosts` 

Service|Hostname|Port number
------|---------|-----------
Webserver|[localhost](http://localhost:8080)|8080

## Hosts within your environment ##

You'll need to configure your application to use any services you enabled:

Service|Hostname|Port number
------|---------|-----------
Webserver|[localhost](http://localhost:8080)|8080
php-fpm|php-fpm|9000
MongoDB|[localhost](http://localhost:27017)|27017

# Docker compose cheatsheet #

**Note:** you need to cd first to where your docker-compose.yml file lives.

  * Start containers in the background: `docker-compose up -d`
  * Start containers on the foreground: `docker-compose up`. You will see a stream of logs for every container running.
  * Stop containers: `docker-compose stop`
  * Kill containers: `docker-compose kill`
  * View container logs: `docker-compose logs`
  * Execute command inside of container: `docker-compose exec SERVICE_NAME COMMAND` where `COMMAND` is whatever you want to run. Examples:
        * Shell into the PHP container, `docker-compose exec php-fpm bash`
        * Run symfony console, `docker-compose exec php-fpm bin/console`
        * Open a mysql shell, `docker-compose exec mysql mysql -uroot -pCHOSEN_ROOT_PASSWORD`

License
----

MIT

